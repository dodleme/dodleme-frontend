import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import {AuthenticationService} from '../authentication.service'
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  errorMessage = "";
  constructor(private authService: AuthenticationService) {

  }

  ngOnInit() {
  }
  onSignUp(formModel: NgForm): void{


    const prenom = formModel.form.value.prenom;
    const nom = formModel.form.value.nom;

    const password = formModel.form.value.password;

    this.authService.signUp(nom, prenom, password).subscribe(
      x => {
        console.log(x);
        if(x.token == undefined) {
          window.location.replace('signIn');
        }

      }
      ,
      err => {
        if(err instanceof HttpErrorResponse){
          console.log(err);
        }

      }
    );
  }
}
