export class UtilisateurLog {
  id: string;
  nom: string;
  prenom: string;
  token: string;
  message: string;
}
