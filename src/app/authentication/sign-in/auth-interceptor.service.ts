import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { AuthenticationService } from '../authentication.service';

/**
 * A chaque requete cette classe intervient pour envoyer notre token de connexion au serveur
 * pour lui signaler que nous sommes bien authentifiés
 */
@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  constructor( private authService: AuthenticationService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const token = this.authService.getToken();
    const authRequest = req.clone({
      headers: req.headers.set('Authorization', 'Bearer ' + token)
    });
    return next.handle(authRequest);
  }
}
