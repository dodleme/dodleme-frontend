import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthenticationService} from '../authentication.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  errorMessage = '';
  constructor(private  authService: AuthenticationService) { }

  ngOnInit() {
  }

  onSignIn(formModel: NgForm) {
    const nom = formModel.value.nom;
    const prenom = formModel.value.prenom;
    const password = formModel.value.password;

    this.authService.signIn(nom, prenom, password).subscribe(x => {
      if(!this.authService.isLogged()){
        this.errorMessage = 'Erreur de connexion';
      }
    });

  }
}
