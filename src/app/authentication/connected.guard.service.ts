import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import { AuthenticationService } from './authentication.service';

/**
 * Class singleton permettant d'autoriser les accès à des pages que si l'utilisateur est connecté
 */
@Injectable({
  providedIn: 'root'
})
export class ConnectedGuardService implements CanActivate {

  constructor(public authService: AuthenticationService, public router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    if (!this.authService.isLogged()) {
      this.router.navigate(['signIn']);
      return false;
    }
    return true;
  }
}
