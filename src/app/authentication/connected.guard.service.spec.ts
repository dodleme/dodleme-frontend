import { TestBed } from '@angular/core/testing';

import { Connected.GuardService } from './connected.guard.service';

describe('Connected.GuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Connected.GuardService = TestBed.get(Connected.GuardService);
    expect(service).toBeTruthy();
  });
});
