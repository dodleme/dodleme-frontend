import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Utilisateur } from './Utilisateur';
import {catchError, tap} from 'rxjs/operators';
import {UtilisateurLog} from './UtilisateurLog';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private userLogged: Utilisateur = null;
  private url: string;
  private token: string;
  private expiration: Date;
  private helper = new JwtHelperService();
  private urlR: string = '/evenements';


  /**
   * Methode permettant de configurer l'url de redirection après authentification
   * @param pUrl
   */
  public setUrl(pUrl: string): void {
    this.urlR = pUrl;
  }

  /**
   * Methode permettant de savoir si l'utiliteur est authentifié ou non
   */
  isLogged(): boolean {
    const token: string = localStorage.getItem('token');
    if(token != undefined) {
      const isLogged = !this.helper.isTokenExpired(token);

      return isLogged;
    } else{
      return false;
    }

  }

  /**
   * Methode pour obtenir le token retenu en cache
   */
  getToken(): string {
    if (this.isLogged()) {
      const token: string = localStorage.getItem('token');
      return token;
    }
    return '';
  }

  /**
   * methode permettant de sauvegarder les informations de connexions en cache
   * @param pToken
   * @param pUser
   */
  saveAuthData(pToken: string, pUser: Utilisateur): void {
    localStorage.setItem('token', pToken);
    localStorage.setItem('nom', pUser.nom);
    localStorage.setItem('prenom', pUser.prenom);
    localStorage.setItem('id', pUser.id);
  }

  clearAuthData() {
    localStorage.removeItem('token');
    localStorage.removeItem('username');
    localStorage.removeItem('email');
    localStorage.removeItem('id');
  }

  getUserLoggedId() {
    return localStorage.getItem('id');
  }

  logout() {
    this.clearAuthData();
    this.token = '';
    this.userLogged = null;
  }


  constructor(private http: HttpClient) {
    this.url = 'http://127.0.0.1:3000';
  }

  private handleError<T>(operation: string = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(error);
      console.log(`${operation} failed: ${error.message}`);
      return of( result as T ); //l'objet reseulte et passé en param
    } ; //On retourne un obserbble en cas d'erreur pour que l'appli continue quoi qu'il arrive
  }

  /**
   * Methode permettant de s'inscrire
   * @param pNom
   * @param pPrenom
   * @param mdp
   */
  signUp(pNom: string, pPrenom: string, mdp: string) {
    // tslint:disable-next-line:max-line-length
    console.log(pNom + ' ' + pPrenom + ' ' + mdp );
    return this.http.post<UtilisateurLog>(this.url + '/auth',
      {nom: pNom, prenom: pPrenom, password: mdp}).pipe(
      catchError(this.handleError<UtilisateurLog>('signUp'))
    );
  }
  signIn(pNom: string, pPrenom: string, mdp: string) {
    return this.http.post<UtilisateurLog>(this.url + '/auth/signIn', {nom: pNom, prenom: pPrenom, password: mdp}).pipe(
      tap(Response => {
          this.userLogged = {
            id: Response.id,
            nom: Response.nom,
            prenom: Response.prenom
          };
          this.saveAuthData(Response.token, this.userLogged);
          console.log(Response);
          window.location.replace('/');
        }
        ),

      catchError(this.handleError<UtilisateurLog>('signIn'))

    );
  }

}
