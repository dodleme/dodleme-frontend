import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { SignInComponent } from '../authentication/sign-in/sign-in.component';
import { SignUpComponent } from '../authentication/sign-up/sign-up.component';
import { NgModule } from '@angular/core';
import {ListeEvenementsComponent} from '../evenement/liste-evenements/liste-evenements.component';
import {ConnectedGuardService} from '../authentication/connected.guard.service';
import {CreerEvenementComponent} from '../evenement/creer-evenement/creer-evenement.component';
const appRoutes: Routes = [
    { path: 'home', component: HomeComponent },
    {path: 'signIn', component: SignInComponent},
    {path: 'signUp', component: SignUpComponent},
    {path: 'evenements/creer', component: CreerEvenementComponent, canActivate: [ConnectedGuardService]},
    {path: 'evenements', component: ListeEvenementsComponent, canActivate: [ConnectedGuardService]},
    { path: '**', redirectTo: 'home', pathMatch: 'full' },

  ];

  @NgModule({
    imports: [
      RouterModule.forRoot(
        appRoutes
      )
      // other imports here
    ],
    exports: [RouterModule]
  })
  export class AppRouterModule { }
