import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRouterModule } from './routes/routes.module';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { SignUpComponent } from './authentication/sign-up/sign-up.component';
import { SignInComponent } from './authentication/sign-in/sign-in.component';
import {FormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import {HttpInterceptorError} from './HttpInterceptorError';
import {ConnectedGuardService} from './authentication/connected.guard.service';
import {AuthenticationService} from './authentication/authentication.service';
import {AuthInterceptorService} from './authentication/sign-in/auth-interceptor.service';
import { CreerEvenementComponent } from './evenement/creer-evenement/creer-evenement.component';
import { ListeEvenementsComponent } from './evenement/liste-evenements/liste-evenements.component';
import { FullCalendarModule } from '@fullcalendar/angular'; // for FullCalendar!




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    SignUpComponent,
    SignInComponent,
    CreerEvenementComponent,
    ListeEvenementsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRouterModule,
    FullCalendarModule

  ], //Dans les providers, c'est là ou on met tout nos services
  providers: [AuthenticationService,
              ConnectedGuardService ,
              {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true },
              {provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorError, multi: true}]
  ,
  bootstrap: [AppComponent]
})
export class AppModule { }
