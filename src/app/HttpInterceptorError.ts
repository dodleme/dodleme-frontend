
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import Swal from 'sweetalert2';

@Injectable()
export class HttpInterceptorError implements HttpInterceptor {

  constructor() {

  }

  intercept(req: HttpRequest<any>, handler: HttpHandler) {

    return handler.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
          const errorMessage = 'Erreur inconnu';
          console.log(error);
          if (error.error.message == undefined) {
            Swal.fire(
              'Error',
              'Erreur interne du serveur!',
              'error'
            );

          }

          return throwError(error);
        }
      )
    );
  }
}

